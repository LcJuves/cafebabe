package com.lcjuves.cafebabe;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class BeanCopierTest {

    @Test
    void testCopyProperties() {
        Boy boy = new Boy("none");
        People people = new People();
        BeanCopier.copyProperties(boy, people);
        assertEquals(boy.isBoy(), people.isBoy());
    }
}
