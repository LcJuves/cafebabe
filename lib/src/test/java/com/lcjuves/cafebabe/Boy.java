package com.lcjuves.cafebabe;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@ToString
public class Boy extends People {

    private String girlFriend;

    public Boy(String girlFriend) {
        this.girlFriend = girlFriend;
        setBoy(true);
    }
}
