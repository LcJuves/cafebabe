package com.lcjuves.cafebabe;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class StringsTest {

    @Test
    void testConvertToJSON() {
        Map<String, String> test = new HashMap<>();
        test.put("name", "Liangcheng Juves");
        test.put("age", "24");
        String json = Strings.convertToJSON(test);
        assertEquals(json, "{\"name\":\"Liangcheng Juves\",\"age\":\"24\"}");
    }
}
