package com.lcjuves.cafebabe;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class Strings {

    public static final String EMPTY = "";
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");

    private Strings() {
        throw new UnsupportedOperationException();
    }

    /**
     * Gives a string consisting of the given character repeated the given
     * number of times.
     *
     * @param ch the character to repeat
     * @param count how many times to repeat the character
     * @return the resultant string
     */
    public static String repeat(char ch, int count) {
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < count; ++i) {
            buffer.append(ch);
        }
        return buffer.toString();
    }

    /**
     * Gives a string consisting of a given string prepended and appended with
     * surrounding characters.
     *
     * @param target a string
     * @param begin character to prepend
     * @param end character to append
     * @return the surrounded string
     */
    public static String surround(String target, char begin, char end) {
        StringBuilder result = new StringBuilder();
        result.append(begin);
        result.append(target);
        result.append(end);
        return result.toString();
    }

    /**
     * Tells whether the given string is either {@code} or consists solely of
     * whitespace characters.
     *
     * @param target string to check
     * @return {@code true} if the target string is null or empty
     */
    public static boolean isNullOrEmpty(String target) {
        return Objects.isNull(target) || target.isEmpty();
    }

    public static List<String> splitToList(String from, String regex) {
        return Arrays.asList(from.split(regex));
    }

    public static String convertToJSON(Map<String, String> strings) {
        List<String> entrySetStringList = strings.entrySet().stream().flatMap(
                (Function<Entry<String, String>, Stream<String>>) entrySet
                -> Stream.of(String.format("\"%s\":\"%s\"",
                        entrySet.getKey(), entrySet.getValue())))
                .collect(Collectors.toList());
        String target = String.join(",", entrySetStringList);
        return surround(target, '{', '}');
    }

}
