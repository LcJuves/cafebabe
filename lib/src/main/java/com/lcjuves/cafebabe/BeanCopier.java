package com.lcjuves.cafebabe;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Create at 2023/10/21 22:49
 *
 * @author Liangcheng Juves
 */
public final class BeanCopier {

    private BeanCopier() {
        throw new UnsupportedOperationException();
    }

    public static void copyProperties(Object from, Object to) {
        BeanCopierHandler handler = new BeanCopierHandler() {
            @Override
            public void onNext(Class<?> fromFieldType, Field fromField,
                    Class<?> toFieldType, Field toField) {
                if (fromFieldType == toFieldType
                        || isPrimitiveMatched(fromFieldType, toFieldType)
                        || isWrapperMatched(fromFieldType, toFieldType)) {
                    try {
                        Object toSet = fromField.get(from);
                        if (toSet != null) {
                            toField.set(to, toSet);
                        }
                    } catch (IllegalArgumentException | IllegalAccessException e) {
                        this.onSkippedException(e);
                    }
                }
            }

            @Override
            public void onSkippedException(Exception e) {
                // TODO: Log the error messages
            }
        };
        copyProperties(from, to, handler);
    }

    public static boolean isStringCanConvert(Class<?> fromFieldType, Class<?> toFieldType) {
        return fromFieldType == String.class
                && ((toFieldType.isPrimitive() || Primitives.isWrapperType(toFieldType))
                || toFieldType == String[].class);
    }

    public static Object convertStringToSupportTypes() {
        return new Object();
    }

    public static boolean isPrimitiveMatched(Class<?> fromFieldType, Class<?> toFieldType) {
        return fromFieldType.isPrimitive()
                && Primitives.isWrapperType(toFieldType)
                && Primitives.unwrap(toFieldType) == fromFieldType;
    }

    public static boolean isWrapperMatched(Class<?> fromFieldType, Class<?> toFieldType) {
        return Primitives.isWrapperType(fromFieldType)
                && toFieldType.isPrimitive()
                && Primitives.wrap(toFieldType) == fromFieldType;
    }

    public static void copyProperties(Object from, Object to, BeanCopierHandler handler) {
        Objects.requireNonNull(from, "from == null");
        Objects.requireNonNull(to, "to == null");
        Objects.requireNonNull(handler, "handler == null");

        Class<?> fromClass = from.getClass();
        List<Field> fromFields = new ArrayList<>();
        fromFields.addAll(Arrays.asList(fromClass.getDeclaredFields()));
        while (true) {
            final Class<?> fromSuperClass = fromClass.getSuperclass();
            if (fromSuperClass == Object.class) {
                break;
            }
            fromFields.addAll(Arrays.asList(fromSuperClass.getDeclaredFields()));
            fromClass = fromSuperClass;
        }
        fromFields = fromFields.stream().distinct().collect(Collectors.toList());
        for (Field fromField : fromFields) {
            fromField.setAccessible(true);
            String fromFieldName = fromField.getName();
            Field toField = null;
            try {
                toField = to.getClass().getDeclaredField(fromFieldName);
                toField.setAccessible(true);
                Class<?> fromFieldType = fromField.getType();
                Class<?> toFieldType = toField.getType();
                handler.onNext(fromFieldType, fromField, toFieldType, toField);
            } catch (NoSuchFieldException | SecurityException e) {
                handler.onSkippedException(e);
            } finally {
                fromField.setAccessible(false);
                if (toField != null) {
                    toField.setAccessible(false);
                }
            }
        }
    }
}
