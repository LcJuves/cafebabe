package com.lcjuves.cafebabe;

import java.lang.reflect.Field;

/**
 * Created at 2023/10/29 12:54
 *
 * @author Liangcheng Juves
 */
public interface BeanCopierHandler {

    void onNext(Class<?> fromFieldType, Field fromField,
            Class<?> toFieldType, Field toField);

    void onSkippedException(Exception e);
}
